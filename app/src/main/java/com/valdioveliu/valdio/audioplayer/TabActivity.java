package com.valdioveliu.valdio.audioplayer;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TabActivity extends AppCompatActivity {

    private Button start,status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        start = (Button) findViewById(R.id.start);
        status = (Button) findViewById(R.id.status);

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(MediaPlayerService.getmPlayBackStatus() == -1 || MediaPlayerService.getmPlayBackStatus() == 3)
        {
            status.setText("Stopped");
        }else
        {
            status.setText("Music going on");
        }

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status.getText().equals("Music going on"))
                {
                    if(MediaPlayerService.getmPlayBackStatus() == 1)
                    {
                        MediaPlayerService.obj.pauseMedia();
                        MainActivity.setPlayBackStatus("Resume");
                    }else if(MediaPlayerService.getmPlayBackStatus() == 2){
                        MediaPlayerService.obj.resumeMedia();
                        MainActivity.setPlayBackStatus("Pause");
                    }
                }
            }
        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TabActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
