package com.valdioveliu.valdio.audioplayer;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.os.Build.VERSION.SDK_INT;
import static com.valdioveliu.valdio.audioplayer.MediaPlayerService.getmPlayBackStatus;
import static com.valdioveliu.valdio.audioplayer.MediaPlayerService.setmPlayBackStatus;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static final String Broadcast_PLAY_NEW_AUDIO = "com.valdioveliu.valdio.audioplayer.PlayNewAudio";

    private MediaPlayerService playerService;
    boolean serviceBound = false;
    ArrayList<Audio> audioList;
    SeekBar playerSeekBar;
    ImageView collapsingImageView;

    int imageIndex = 0;
    private static ImageView btn_play_pause,btn_stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initViews();

        setupSeekbar();
        imageChanger();

        if (checkAndRequestPermissions()) {
            loadAudioList();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                playAudio("https://upload.wikimedia.org/wikipedia/commons/6/6c/Grieg_Lyric_Pieces_Kobold.ogg");
                //play the first audio in the ArrayList
//                playAudio(2);
                if (imageIndex == 4) {
                    imageIndex = 0;
                    loadCollapsingImage(imageIndex);
                } else {
                    loadCollapsingImage(++imageIndex);
                }
            }
        });
    }

    private void initViews() {
        collapsingImageView = (ImageView) findViewById(R.id.collapsingImageView);

        //loadCollapsingImage(imageIndex);

        btn_play_pause = (ImageView) findViewById(R.id.playMediaBtn);
        btn_stop = (ImageView) findViewById(R.id.stop_media);

        btn_play_pause.setOnClickListener(this);
        btn_stop.setOnClickListener(this);

        playerSeekBar = (SeekBar) findViewById(R.id.playerSeekBar);
    }


    private void setupSeekbar() {
        final Handler mHandler1 = new Handler();
        //Make sure you update Seekbar on UI thread
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (getmPlayBackStatus()==1 || getmPlayBackStatus()==2||getmPlayBackStatus()==3) {
                        if (MediaPlayerService.MEDIA_DURATION == 0) {
                            //Toast.makeText(getApplicationContext(), String.valueOf(MediaPlayerService.MEDIA_DURATION), Toast.LENGTH_SHORT).show();
                        } else {
                            mHandler1.removeCallbacksAndMessages(null);
                            playerSeekBar.setMax(MediaPlayerService.MEDIA_DURATION / 1000);
                        }
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                mHandler1.postDelayed(this, 1000);
            }
        });


        final Handler mHandler = new Handler();
        //Make sure you update Seekbar on UI thread
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (getmPlayBackStatus()==1 || getmPlayBackStatus()==2||getmPlayBackStatus()==3) {
                    int mCurrentPosition = playerService.getCurrentPositionSForSeek() / 1000;
                    //Toast.makeText(getApplicationContext(), "Curr" + mCurrentPosition + "/" + playerSeekBar.getMax() / 1000, Toast.LENGTH_SHORT).show();
                    playerSeekBar.setProgress(mCurrentPosition);
                    if (playerSeekBar.getMax() == playerSeekBar.getProgress()) {
                        mHandler.removeCallbacksAndMessages(null);
                    }
                }else{

                    //btn_play_pause.setImageResource(android.R.drawable.ic_media_play);
                    setPlayPauseImage(true,false);
                }


                mHandler.postDelayed(this, 1000);
            }
        });

        playerSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (serviceBound && fromUser) {
                    try {
                        playerService.seekTo(progress * 1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }


    int currentPage = 0;
    private void imageChanger() {
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 0)
                    collapsingImageView.setBackground(getResources().getDrawable(R.drawable.image1));
                if (currentPage == 1)
                    collapsingImageView.setBackground(getResources().getDrawable(R.drawable.image2));
                if (currentPage == 2)
                    collapsingImageView.setBackground(getResources().getDrawable(R.drawable.image3));
                if (currentPage == 3)
                    collapsingImageView.setBackground(getResources().getDrawable(R.drawable.image4));
                if (currentPage == 4) {
                    collapsingImageView.setBackground(getResources().getDrawable(R.drawable.image5));
                    currentPage = -1;
                }
                currentPage++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 9000);
    }





    private void loadAudioList() {
        loadAudio();
        initRecyclerView();
        if(getmPlayBackStatus() == -1 || getmPlayBackStatus() == 3)
            playAudio(0);
    }

    private boolean checkAndRequestPermissions() {
        if (SDK_INT >= Build.VERSION_CODES.M) {
            int permissionReadPhoneState = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            List<String> listPermissionsNeeded = new ArrayList<>();

            if (permissionReadPhoneState != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }

            if (permissionStorage != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                return false;
            } else {
                return true;
            }
        }else
            loadAudioList();
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        String TAG = "LOG_PERMISSION";
        Log.d(TAG, "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions

                    if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            ) {
                        Log.d(TAG, "Phone state and storage permissions granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                        loadAudioList();
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                      //shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                            showDialogOK("Phone state and storage permissions required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }


    private void initRecyclerView() {
        if (audioList != null && audioList.size() > 0) {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
            RecyclerView_Adapter adapter = new RecyclerView_Adapter(audioList, getApplication());
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addOnItemTouchListener(new CustomTouchListener(this, new onItemClickListener() {
                @Override
                public void onClick(View view, int index) {
                    //playAudio(index);
                }
            }));
        }
    }

    private void loadCollapsingImage(int i) {
        TypedArray array = getResources().obtainTypedArray(R.array.images);
        collapsingImageView.setImageDrawable(array.getDrawable(i));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putBoolean("serviceStatus", serviceBound);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("serviceStatus");
    }
    static void setPlayPauseImage(boolean play,boolean isResume)
    {
        if (play) {
            btn_play_pause.setImageResource(android.R.drawable.ic_media_play);
        } else {
            btn_play_pause.setImageResource(android.R.drawable.ic_media_pause);
        }

        if (isResume) {
            btn_play_pause.setImageResource(android.R.drawable.ic_media_play);
        }
    }
    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            playerService = binder.getService();
            serviceBound = true;

            if(playerService!=null)
                if (playerService.getmPlayBackStatus() == 1) {
                    //btn_play_pause.setText("Pause");
                    setPlayPauseImage(false,false);
                    playerService.buildNotification(PlaybackStatus.PAUSED);
                }
                else if (playerService.getmPlayBackStatus() == 2) {
                    //btn_play_pause.setText("Resume");
                    setPlayPauseImage(false,true);
                    playerService.buildNotification(PlaybackStatus.PLAYING);
                }
                else if(playerService.getmPlayBackStatus() == 3)
                {
                    //btn_play_pause.setText("Play");
                    setPlayPauseImage(true,false);
                }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        Log.e("STATUS",getmPlayBackStatus()+"");
        Intent playerIntent = new Intent(this, MediaPlayerService.class);
        bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        if(getmPlayBackStatus() == 3)
            playAudio(0);

    }

    private void playAudio(int audioIndex) {
        //Check is service is active
        if (!serviceBound ) {
            Log.e("Play", "Play");
            //Store Serializable audioList to SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudio(audioList);
            storage.storeAudioIndex(audioIndex);

            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
//            btn_play_pause.setText("Pause");
            setPlayPauseImage(false,false);
        } else {
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudio(audioList);
            if (storage.loadAudioIndex() == audioIndex) {
                if (playerService.getmPlayBackStatus() == 1) {
                    playerService.pauseMedia();
                    //btn_play_pause.setText("Resume");
                    setPlayPauseImage(false,true);
                    playerService.buildNotification(PlaybackStatus.PAUSED);
                }
                else if (playerService.getmPlayBackStatus() == 2) {
                    //btn_play_pause.setText("Pause");
                    setPlayPauseImage(false,false);
                    playerService.resumeMedia();
                    playerService.buildNotification(PlaybackStatus.PLAYING);
                }
                else if(playerService.getmPlayBackStatus() == 3)
                {
                    //btn_play_pause.setText("Pause");
                    setPlayPauseImage(false,false);
                    Log.e("Stopped", "New");
                    //Store the new audioIndex to SharedPreferences
                    storage.storeAudioIndex(audioIndex);
                    //Service is active
                    //Send a broadcast to the service -> PLAY_NEW_AUDIO
                    Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
                    sendBroadcast(broadcastIntent);
                    playerService.buildNotification(PlaybackStatus.PAUSED);
                }else
                {
                    Log.e("New", "New");
                    storage.storeAudio(audioList);
                    //Store the new audioIndex to SharedPreferences
                    storage.storeAudioIndex(audioIndex);
                    //Service is active
                    //Send a broadcast to the service -> PLAY_NEW_AUDIO
                    Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
                    sendBroadcast(broadcastIntent);
                }

            } else {
                Log.e("New", "New");
                storage.storeAudio(audioList);
                //Store the new audioIndex to SharedPreferences
                storage.storeAudioIndex(audioIndex);
                //Service is active
                //Send a broadcast to the service -> PLAY_NEW_AUDIO
                Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
                sendBroadcast(broadcastIntent);
            }

        }
    }

    /**
     * Load audio files using {@link ContentResolver}
     * <p>
     * If this don't works for you, load the audio files to audioList Array your oun way
     */
    private void loadAudio() {
        ContentResolver contentResolver = getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";

        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            audioList = new ArrayList<>();
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioList
                audioList.add(new Audio(data, title, album, artist));
            }
        }
        if (cursor != null)
            cursor.close();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (serviceBound) {
            unbindService(serviceConnection);
            //service is active
            //playerService.stopSelf();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.playMediaBtn:
                playAudio(0);
                break;
            case R.id.stop_media:
                playerService.stopMedia();
                //btn_play_pause.setText("Play");
                setPlayPauseImage(true,false);
                playerService.removeNotification();
                setmPlayBackStatus(3);
                break;
        }
    }

    public static void setPlayBackStatus(String text)
    {
        //btn_play_pause.setText(text);
        if(text.equalsIgnoreCase("play"))
            setPlayPauseImage(true,false);
        else if(text.equalsIgnoreCase("pause"))
            setPlayPauseImage(false,false);
        else if(text.equalsIgnoreCase("resume"))
            setPlayPauseImage(false,true);


    }
}
